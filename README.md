# JAVA开发最佳实践

## 概述

该项目是本人根据多年的工作经验总结出来的一些常用功能最佳的实现方式。
由于本人能力有限，如果大家有更好的实践方式欢迎提交Issues一起探讨。

## 目录说明

| 目录                         | 说明      | 应用场景                      |
|----------------------------|---------|---------------------------|
| [GeneratePDF](GeneratePDF) | 生成PDF文件 | 通常用于生成单据，如：发票，货运单，保单，报销单等 |

## 代码说明

1. DEMO代码使用maven或者gradle构建
2. 代码详情信息，请进入对应目录的README.md查看
3. junit-jupiter作为单元测试框架
4. 没特别说明则使用JDK1.8

## 构建配置
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <configuration>
                <source>1.8</source>
                <target>1.8</target>
                <encoding>UTF-8</encoding>
                <compilerArgument>-parameters</compilerArgument>
            </configuration>
        </plugin>
        <!-- 用于支持junit-jupiter的单元测试 -->
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>3.0.0-M7</version>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-report-plugin</artifactId>
            <version>3.0.0-M7</version>
        </plugin>
    </plugins>
</build>
```

## 更新说明

不定时补充...