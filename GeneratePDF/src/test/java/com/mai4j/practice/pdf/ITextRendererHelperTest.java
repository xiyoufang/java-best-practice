package com.mai4j.practice.pdf;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;


/**
 * 微信公众号请关注: 开源螺丝钉
 * <br>
 * 码云请关注: <a href="https://gitee.com/xiyoufang">https://gitee.com/xiyoufang</a>
 * <br>
 * 哔哩哔哩请关注: <a href="https://space.bilibili.com/438927834">https://space.bilibili.com/438927834</a>
 *
 * @author xiyoufang
 */
class ITextRendererHelperTest {

    private static File tempDir;

    private static File ff;

    @SneakyThrows
    @BeforeAll
    static void initRes() {
        tempDir = new File(FileUtils.getTempDirectory(), "_GeneratePDF");
        FileUtils.forceMkdir(tempDir);
        ff = new File(tempDir, "Kai.ttf");
        FileUtils.copyURLToFile(IOUtils.resourceToURL("/Kai.ttf"), ff);
        Assertions.assertTrue(ff.exists());
    }

    @SneakyThrows
    @AfterAll
    static void deleteRes() {
        FileUtils.deleteDirectory(tempDir);
    }

    @Test
    void get() {
        ITextRenderer renderer = ITextRendererHelper.get();
        Assertions.assertNotNull(renderer);
        Assertions.assertEquals(renderer, ITextRendererHelper.get());
    }

    @Test
    void addFont() {
        ITextFontResolver fontResolver = setupFontResolver();
        // 不存在的字体
        Assertions.assertThrowsExactly(PdfException.class, () -> ITextRendererHelper.addFont("./1.t"));
        Mockito.verify(fontResolver, Mockito.times(0)).getFontFamily("KaiTi_GB2312");
        // 楷体
        Assertions.assertDoesNotThrow(() -> ITextRendererHelper.addFont(ff.getAbsolutePath()));
        Mockito.verify(fontResolver, Mockito.atLeast(1)).getFontFamily("KaiTi_GB2312");
    }

    @Test
    void addFonts() {
        ITextFontResolver fontResolver = setupFontResolver();
        ITextRendererHelper.addFonts(tempDir.getAbsolutePath());
        Mockito.verify(fontResolver, Mockito.atLeast(1)).getFontFamily("KaiTi_GB2312");
    }

    ITextFontResolver setupFontResolver() {
        ITextRenderer textRenderer = ITextRendererHelper.get();
        SharedContext sharedContext = textRenderer.getSharedContext();
        ITextFontResolver fontResolver = Mockito.spy(textRenderer.getFontResolver());
        sharedContext.setFontResolver(fontResolver);
        return fontResolver;
    }
}