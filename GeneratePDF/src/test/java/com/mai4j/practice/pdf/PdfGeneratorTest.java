package com.mai4j.practice.pdf;

import com.jfinal.kit.Kv;
import com.jfinal.template.Engine;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.*;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * 微信公众号请关注: 开源螺丝钉
 * <br>
 * 码云请关注: <a href="https://gitee.com/xiyoufang">https://gitee.com/xiyoufang</a>
 * <br>
 * 哔哩哔哩请关注: <a href="https://space.bilibili.com/438927834">https://space.bilibili.com/438927834</a>
 *
 * @author xiyoufang
 */
class PdfGeneratorTest {


    private static File lg;

    private static File tempDir;

    @SneakyThrows
    @BeforeAll
    static void initRes() {
        tempDir = new File(FileUtils.getTempDirectory(), "_GeneratePDF");
        FileUtils.forceMkdir(tempDir);
        File ff = new File(tempDir, "Kai.ttf");
        lg = new File(tempDir, "logo.png");
        FileUtils.copyURLToFile(IOUtils.resourceToURL("/Kai.ttf"), ff);
        FileUtils.copyURLToFile(IOUtils.resourceToURL("/logo.png"), lg);
        ITextRendererHelper.addFont(ff.getAbsolutePath());
    }

    @SneakyThrows
    @AfterAll
    static void deleteRes() {
        FileUtils.deleteDirectory(tempDir);
    }

    @SneakyThrows
    @Test
    void createPdf() {
        File pf = new File(tempDir, "page_100x150mm.pdf");
        PdfGenerator.createPdf(Files.newOutputStream(pf.toPath()),
                IOUtils.resourceToString("/page_100x150mm.html", StandardCharsets.UTF_8));
        String actualTextFromPage = PdfHelper.getTextFromPage(Files.newInputStream(pf.toPath()), 1);
        String expectedTextFromPage = PdfHelper.getTextFromPage(IOUtils.resourceToURL("/page_100x150mm.pdf").openStream(), 1);
        Assertions.assertEquals(expectedTextFromPage, actualTextFromPage);
        Assertions.assertEquals(IOUtils.resourceToByteArray("/page_100x150mm.pdf").length, pf.length());
        int[] sizeFromPage = PdfHelper.getSizeFromPage(Files.newInputStream(pf.toPath()), 1);
        Assertions.assertArrayEquals(new int[]{100, 150}, sizeFromPage);
    }

    @SneakyThrows
    @Test
    @DisplayName("使用模板生成PDF")
    void createPdfFromTemplate() {
        File pf = new File(tempDir, "page_with_logo.pdf");
        String html = Engine.use().getTemplateByString(IOUtils.resourceToString("/page_with_logo.html", StandardCharsets.UTF_8))
                .renderToString(Kv.create().set("logo", lg.getAbsolutePath()));
        PdfGenerator.createPdf(Files.newOutputStream(pf.toPath()), html);
        String actualTextFromPage = PdfHelper.getTextFromPage(Files.newInputStream(pf.toPath()), 1);
        String expectedTextFromPage = PdfHelper.getTextFromPage(IOUtils.resourceToURL("/page_with_logo.pdf").openStream(), 1);
        Assertions.assertEquals(expectedTextFromPage, actualTextFromPage);
        Assertions.assertEquals(IOUtils.resourceToByteArray("/page_with_logo.pdf").length, pf.length());
    }

    @SneakyThrows
    @Test
    @DisplayName("不指定字体")
    void createPdfWithoutFont() {
        File pf = new File(tempDir, "page_without_font.pdf");
        PdfGenerator.createPdf(Files.newOutputStream(pf.toPath()), IOUtils.resourceToString("/page_without_font.html", StandardCharsets.UTF_8));
        String actualTextFromPage = PdfHelper.getTextFromPage(Files.newInputStream(pf.toPath()), 1);
        String expectedTextFromPage = PdfHelper.getTextFromPage(IOUtils.resourceToURL("/page_100x150mm.pdf").openStream(), 1);
        Assertions.assertNotEquals(expectedTextFromPage, actualTextFromPage);
        Assertions.assertFalse(actualTextFromPage.contains("顾客信息"));
    }

}