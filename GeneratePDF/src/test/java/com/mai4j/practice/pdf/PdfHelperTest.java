package com.mai4j.practice.pdf;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;

/**
 * 微信公众号请关注: 开源螺丝钉
 * <br>
 * 码云请关注: <a href="https://gitee.com/xiyoufang">https://gitee.com/xiyoufang</a>
 * <br>
 * 哔哩哔哩请关注: <a href="https://space.bilibili.com/438927834">https://space.bilibili.com/438927834</a>
 *
 * @author xiyoufang
 */
class PdfHelperTest {

    private static File tempDir;

    @SneakyThrows
    @BeforeAll
    static void initRes() {
        tempDir = new File(FileUtils.getTempDirectory(), "_GeneratePDF");
        FileUtils.forceMkdir(tempDir);
    }


    @SneakyThrows
    @AfterAll
    static void deleteRes() {
        FileUtils.deleteDirectory(tempDir);
    }

    @SneakyThrows
    @Test
    void mergePdf() {
        File pf = new File(tempDir, "all-in-one.pdf");
        Assertions.assertDoesNotThrow(() -> PdfHelper.mergePdf(Files.newOutputStream(pf.toPath()), IOUtils.resourceToURL("/page_100x150mm.pdf").openStream(),
                IOUtils.resourceToURL("/page_a5.pdf").openStream(),
                IOUtils.resourceToURL("/page_a4.pdf").openStream(),
                IOUtils.resourceToURL("/page_with_logo.pdf").openStream()
        ));
        Assertions.assertEquals(4, PdfHelper.getNumberOfPages(Files.newInputStream(pf.toPath())));
    }

    @SneakyThrows
    @Test
    void getNumberOfPages() {
        int numberOfPages = PdfHelper.getNumberOfPages(IOUtils.resourceToURL("/all-in-one.pdf").openStream());
        Assertions.assertEquals(4, numberOfPages);
    }

    @SneakyThrows
    @Test
    void getTextFromPage() {
        String textFromPage = PdfHelper.getTextFromPage(IOUtils.resourceToURL("/page_100x150mm.pdf").openStream(), 1);
        Assertions.assertTrue(textFromPage.contains("Order Information Tag"));
        Assertions.assertTrue(textFromPage.contains("顾客信息"));
    }

    @SneakyThrows
    @Test
    void getSizeFromPage() {
        int[] sizeFromPage = PdfHelper.getSizeFromPage(IOUtils.resourceToURL("/page_100x150mm.pdf").openStream(), 1);
        Assertions.assertArrayEquals(new int[]{100, 150}, sizeFromPage);
    }
}