package com.mai4j.practice.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 微信公众号请关注: 开源螺丝钉
 * <br>
 * 码云请关注: <a href="https://gitee.com/xiyoufang">https://gitee.com/xiyoufang</a>
 * <br>
 * 哔哩哔哩请关注: <a href="https://space.bilibili.com/438927834">https://space.bilibili.com/438927834</a>
 *
 * @author xiyoufang
 */
@Slf4j
public class PdfHelper {

    /**
     * 合并Pdf
     *
     * @param out 输出
     * @param ins 输入
     * @throws PdfException PdfException
     */
    public static void mergePdf(OutputStream out, InputStream... ins) throws PdfException {
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        try {
            PdfCopy copy = null;
            try {
                copy = new PdfCopy(document, out);
                document.open();
                for (InputStream in : ins) {
                    PdfReader reader = new PdfReader(in);
                    try {
                        copy.addDocument(reader);
                        copy.freeReader(reader);
                    } finally {
                        reader.close();
                    }
                }
            } finally {
                if (document.isOpen())
                    document.close();
                if (copy != null)
                    copy.close();
            }
        } catch (Exception e) {
            throw new PdfException("合并PDF错误！", e);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * 获取PDF页码
     *
     * @param in in
     * @return number of pages
     */
    public static int getNumberOfPages(InputStream in) throws PdfException {
        try {
            PdfReader pdfReader = new PdfReader(in);
            return pdfReader.getNumberOfPages();
        } catch (IOException e) {
            throw new PdfException("获取PDF页码异常", e);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**
     * 获取指定页面的内容
     *
     * @param in   in
     * @param page page
     * @return text
     */
    public static String getTextFromPage(InputStream in, int page) throws PdfException {
        try {
            PdfReader pdfReader = new PdfReader(in);
            return PdfTextExtractor.getTextFromPage(pdfReader, page);
        } catch (IOException e) {
            throw new PdfException("获取PDF内容异常", e);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**
     * 获取指定页面的大小，单位mm
     *
     * @param in   in
     * @param page page
     * @return size array [w * h]
     */
    public static int[] getSizeFromPage(InputStream in, int page) throws PdfException {
        try {
            PdfReader pdfReader = new PdfReader(in);
            Document document = new Document(pdfReader.getPageSize(page));
            float width = document.getPageSize().getWidth();
            float height = document.getPageSize().getHeight();
            double d = 0.352778;
            return new int[]{
                    BigDecimal.valueOf(d * width).setScale(0, RoundingMode.HALF_UP).intValue(),
                    BigDecimal.valueOf(d * height).setScale(0, RoundingMode.HALF_UP).intValue()
            };
        } catch (IOException e) {
            throw new PdfException("获取PDF页面大小异常", e);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }
}
