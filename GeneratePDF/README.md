# 生成PDF

通过模板文件生成HTML，在通过HTML转成PDF。

所以项目主要依赖2个库，一个是HTML转成PDF的库，一个是模板引擎库

## 依赖的JAR库

### HTML转成PDF的库

```xml
<!-- https://mvnrepository.com/artifact/org.xhtmlrenderer/flying-saucer-pdf-itext5 -->
<dependency>
    <groupId>org.xhtmlrenderer</groupId>
    <artifactId>flying-saucer-pdf-itext5</artifactId>
    <version>9.1.22</version>
</dependency>
```

### 模板引擎库

该库有很多可选的，例如: FreeMarker，Thymeleaf，enjoy 等等。

可以根据自己项目实际情况选择，如果已经使用了模板引擎那就没必要引入额外的。

DEMO使用的模板引擎

```xml
<!-- https://mvnrepository.com/artifact/com.jfinal/enjoy -->
<dependency>
    <groupId>com.jfinal</groupId>
    <artifactId>enjoy</artifactId>
    <version>5.0.0</version>
</dependency>
```

## 额外关注...

1. 字体配置
2. 带图片的PDF生成
3. 如何生成不同页面尺寸的PDF，如: A5, A4，指定大小